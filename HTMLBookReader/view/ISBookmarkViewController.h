//
//  ISBookmarkViewController.h
//  HTMLBookReader
//
//  Created by celon on 7/11/12.
//  Copyright (c) 2012 celon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISBookmarkViewController : UITableViewController

@property (atomic, retain) NSString *bookName;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editSwitchButton;

+ (NSNumber *) getSelectedProgress;
- (IBAction)switchEditMode:(id)sender;

@end
