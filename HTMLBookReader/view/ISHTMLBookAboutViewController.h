//
//  ISHTMLBookAboutViewController.h
//  HTMLBookReader
//
//  Created by ziwei yang on 11/15/12.
//  Copyright (c) 2012 celon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISHTMLBookAboutViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *contentWebview;
@property (weak, nonatomic) IBOutlet UIWebView *offlineContentWebview;
@end
