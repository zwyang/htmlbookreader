//
//  ISHTMLBookAboutViewController.m
//  HTMLBookReader
//
//  Created by ziwei yang on 11/15/12.
//  Copyright (c) 2012 celon. All rights reserved.
//

#import "ISHTMLBookAboutViewController.h"

@interface ISHTMLBookAboutViewController () {
    UIBarStyle previousTopBarStyle;
    BOOL previousToolbarHidden;
}

@end

@implementation ISHTMLBookAboutViewController

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [[self offlineContentWebview] setHidden:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //change bar style
    previousTopBarStyle = [self navigationController].navigationBar.barStyle;
    previousToolbarHidden = [[self navigationController].toolbar isHidden];
    [[self navigationController].navigationBar setBarStyle:UIBarStyleBlackOpaque];
    [[self navigationController] setToolbarHidden:YES animated:YES];
    //init web view, load default page first
    NSURL *baseURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]];
    NSString *originalHTMLContent = [[NSBundle mainBundle] pathForResource:@"ebooks_about" ofType:@"html"];
    NSData *htmlData = [NSData dataWithContentsOfFile:originalHTMLContent];
    [[self offlineContentWebview] loadData:htmlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:baseURL];
    //Try load from web
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [[self contentWebview] loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://zwyang-test-apps.googlecode.com/files/ebooks_about.html"]]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[self navigationController].navigationBar setBarStyle:previousTopBarStyle];
    [[self navigationController] setToolbarHidden:previousToolbarHidden animated:YES];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setContentWebview:nil];
    [self setOfflineContentWebview:nil];
    [super viewDidUnload];
}
@end
