//
//  ISHTMLBookViewController.m
//  HTMLBookReader
//
//  Created by celon on 6/11/12.
//  Copyright (c) 2012 celon. All rights reserved.
//

#import "ISHTMLBookViewController.h"
#import "ISBookProgress.h"
#import "ISBookProgressBO.h"
#import "ISBookmarkViewController.h"
#import "MBProgressHUD.h"
#import "SNPopupView.h"
#import "ResourcesUtil.h"

//Wait SEC_X / SEC_Y after changing font
#define SEC_X 1
#define SEC_Y 2
#define SAVE_PROGRESS_INTERVAL 3

@interface ISHTMLBookViewController () {
    BOOL naviBarHidden;
    float progress;
    float fontSize;
    NSTimer *recordProgressTimer;
    NSString *originalHTMLContent;
    MBProgressHUD *hud;
    UIView *customFontView;
    SNPopupView *microPopupFontView;
}

@end

@implementation ISHTMLBookViewController

@synthesize bookName = _bookName;

#pragma mark - GUI Actions

- (IBAction)showFontController:(id)sender {
    if (!sender || ![sender isKindOfClass:[UIBarButtonItem class]])
        return;
    CGPoint point = [self fontControllerPointLabel].frame.origin;
    if (microPopupFontView == nil) {
        microPopupFontView = [[SNPopupView alloc] initWithContentView:[self customFontView] contentSize:[self customFontView].frame.size];
        [microPopupFontView showAtPoint:point inView:self.view
                               animated:YES];
    } else if ([microPopupFontView isDismissed]) {
        [microPopupFontView showAtPoint:point inView:self.view animated:YES];
    } else
        [microPopupFontView dismiss:YES];
}

- (IBAction)increaseFont:(id)sender {
    float targetProgress = self.contentView.scrollView.contentOffset.y / self.contentView.scrollView.contentSize.height;
    
    fontSize *= 1.2f;
    [ResourcesUtil setFontScale:fontSize];
    [self.contentView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'", (int)(fontSize * 100)]];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * SEC_X / SEC_Y);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self recoverProgress:targetProgress];
    });
}

- (IBAction)decreaseFont:(id)sender {
    float targetProgress = self.contentView.scrollView.contentOffset.y / self.contentView.scrollView.contentSize.height;
    
    fontSize /= 1.2f;
    [ResourcesUtil setFontScale:fontSize];
    [self.contentView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'", (int)(fontSize * 100)]];
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * SEC_X / SEC_Y);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self recoverProgress:targetProgress];
    });
}

- (IBAction)saveCurrentProgress:(id)sender {    
    //add loading HUD
    hud.labelText = @"Saving bookmark.";
    // initResourcesTask uses the HUD instance to update progress
    [hud show:YES];
    
    //extract text sentence
    progress = self.contentView.scrollView.contentOffset.y / self.contentView.scrollView.contentSize.height;
    NSString *result = nil;
    float detectScreenOffsetY = 0.f;
    do {
        result = [self.contentView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"var range = document.caretRangeFromPoint(0, %f);range.expand(\"sentence\");range.startContainer.textContent.substring(range.startOffset, range.endOffset);", detectScreenOffsetY]];
        detectScreenOffsetY += 10.f;
        if (detectScreenOffsetY > [[UIScreen mainScreen] bounds].size.height) {
            break;
        }
        if (result)
            result = [result stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    } while (result == nil || [result length] == 0);
    [ISBookProgressBO createBookProgressAt:progress withContext:result inBook:self.bookName];
    NSLog(@"Current progress:%f\nExtracted text:[%@]", progress, result?result:@"NULL");
    
    [hud hide:YES];
}

- (void)recoverProgress:(float)targetProgress {
    [self.contentView.scrollView setContentOffset:CGPointMake(0, self.contentView.scrollView.contentSize.height * targetProgress) animated:YES];
    //flash scroll bar
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC / 3);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.contentView.scrollView flashScrollIndicators];
    });
}

- (IBAction)hideNaviBar:(id)sender {
    naviBarHidden = !naviBarHidden;
    [[self navigationController] setNavigationBarHidden:naviBarHidden animated:YES];
    [[self navigationController] setToolbarHidden:naviBarHidden animated:YES];
}

#pragma mark - custom micro controll view

- (UIView *) customFontView
{
    if (!customFontView) {
        customFontView = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, 160.f, 50.f)];
        UIButton *decreaseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        decreaseBtn.frame = CGRectMake(0.f, 0.f, 60.f, 50.f);
        UIButton *increaseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        increaseBtn.frame = CGRectMake(100.f, 0.f, 60.f, 50.f);
        
        [decreaseBtn setTitle:@"a" forState:UIControlStateNormal];
        [decreaseBtn setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
        decreaseBtn.titleLabel.textAlignment = UITextAlignmentCenter;
        decreaseBtn.titleLabel.font = [UIFont fontWithName:@"Baskerville" size:36.f];
        
        [increaseBtn setTitle:@"A" forState:UIControlStateNormal];
        [increaseBtn setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
        increaseBtn.titleLabel.textAlignment = UITextAlignmentCenter;
        increaseBtn.titleLabel.font = [UIFont fontWithName:@"Baskerville" size:48.f];
        
        [decreaseBtn addTarget:self action:@selector(decreaseFont:) forControlEvents:UIControlEventTouchUpInside];
        [increaseBtn addTarget:self action:@selector(increaseFont:) forControlEvents:UIControlEventTouchUpInside];
        
        [customFontView addSubview:decreaseBtn];
        [customFontView addSubview:increaseBtn];
    }
    return customFontView;
}

#pragma mark - UIWebView delegate methods

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.dimBackground = NO;
    hud.labelText = @"Loading content...";
    hud.detailsLabelText = @"Tap with two fingers to hide toolbar.";
    [hud show:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self navigationController].navigationBar.topItem.title = [self.contentView stringByEvaluatingJavaScriptFromString:@"document.title"];
    //Recover font size
    fontSize = [ResourcesUtil fontScale];
    if (fontSize != 1.0f) {
        [self.contentView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'", (int)(fontSize * 100)]];
    }
    //Recover last progress
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * SEC_X / SEC_Y);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self recoverProgress:[ResourcesUtil recoverProgress]];
    });
    //hide HUD
    if (![hud isHidden]) {
        [hud hide:YES];
    }
    //Add timer to save preferences
    recordProgressTimer = [NSTimer scheduledTimerWithTimeInterval:SAVE_PROGRESS_INTERVAL target:self selector:@selector(recordProgress) userInfo:nil repeats:YES];
}

#pragma mark - recover progress when rotating

- (void)recordProgress
{
    [ResourcesUtil setRecoverProgress:self.contentView.scrollView.contentOffset.y / self.contentView.scrollView.contentSize.height];
}

- (void)recoverAfterRotating
{
    [self recoverProgress:[ResourcesUtil recoverProgress]];
}

#pragma mark - life cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //init HUD
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    // Set determinate mode
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.dimBackground = NO;
    
    //Other attributes
    self.bookName = @"unique_book";
    progress=0.f;
    naviBarHidden = NO;
	// Do any additional setup after loading the view.
    NSURL *baseURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]];
    originalHTMLContent = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"htm"];
    NSData *htmlData = [NSData dataWithContentsOfFile:originalHTMLContent];
    [self.contentView loadData:htmlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:baseURL];
    //Turn off 'scroll to top' feature
    self.contentView.scrollView.scrollsToTop = NO;
    
    //Add auto-recover progress when rotating.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recoverAfterRotating) name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void)delayTime:(NSNumber *)timeInDouble
{
    [NSThread sleepForTimeInterval:[timeInDouble doubleValue]];
}

- (void)viewWillAppear:(BOOL)animated
{
    //load progress
    NSNumber *selectedProgress = [ISBookmarkViewController getSelectedProgress];
    if (selectedProgress) {
        [self recoverProgress:[selectedProgress floatValue]];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setProgressSlider:nil];
    [self setFontControllerPointLabel:nil];
    [super viewDidUnload];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if (self.contentView) {
        progress = self.contentView.scrollView.contentOffset.y / self.contentView.scrollView.contentSize.height;
    }
    return YES;
}

@end
