//
//  ISHTMLBookViewController.h
//  HTMLBookReader
//
//  Created by celon on 6/11/12.
//  Copyright (c) 2012 celon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISHTMLBookViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *contentView;
@property (weak, nonatomic) IBOutlet UISlider *progressSlider;
@property (weak, nonatomic) IBOutlet UILabel *fontControllerPointLabel;

@property (atomic, retain) NSString *bookName;

- (IBAction)showFontController:(id)sender;
- (IBAction)increaseFont:(id)sender;
- (IBAction)decreaseFont:(id)sender;
- (IBAction)saveCurrentProgress:(id)sender;
- (IBAction)hideNaviBar:(id)sender;

@end
