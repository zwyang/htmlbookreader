//
//  ISBookmarkViewController.m
//  HTMLBookReader
//
//  Created by celon on 7/11/12.
//  Copyright (c) 2012 celon. All rights reserved.
//

#import "ISBookmarkViewController.h"
#import "ISBookProgress.h"
#import "ISBookProgressBO.h"

@interface ISBookmarkViewController () {
    UIBarStyle previousTopBarStyle;
    BOOL previousToolbarHidden;
    BOOL editing;
    NSMutableArray *deleteMarks;
}

@end

@implementation ISBookmarkViewController

@synthesize bookName = _bookName;

static NSNumber *selectedProgress = nil;

+ (NSNumber *) getSelectedProgress
{
    return selectedProgress;
}

- (IBAction)switchEditMode:(id)sender {
    editing = !editing;
    [self.editSwitchButton setTitle:editing?@"Delete selected":@"Edit"];
    [self.editSwitchButton setStyle:editing?UIBarButtonItemStyleDone:UIBarButtonItemStyleBordered];
    
    if (editing) {
        //reset switches
        deleteMarks = [NSMutableArray arrayWithCapacity:10];
        NSUInteger count = [[ISBookProgressBO allBookProgressForBook:_bookName] count];
        for (NSUInteger i = 0; i<count; i++) {
            [deleteMarks addObject:[NSNumber numberWithBool:NO]];
        }
    } else {
        [self.tableView beginUpdates];
        //Perform deletion
        NSArray *allProgresses = [ISBookProgressBO allBookProgressForBook:_bookName];
        NSMutableArray *removedRow = [NSMutableArray arrayWithCapacity:10];
        for (NSUInteger i = 0; i<[deleteMarks count]; i++) {
            if ([[deleteMarks objectAtIndex:i] boolValue]) {
                NSLog(@"Delete %d", i);
                [ISBookProgressBO deleteBookmark:[allProgresses objectAtIndex:i]];
                [removedRow addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            }
        }
        [self.tableView deleteRowsAtIndexPaths:removedRow withRowAnimation:YES];
        [self.tableView endUpdates];
    }
    
    [self.tableView setEditing:editing animated:YES];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.bookName = @"unique_book";
    editing = NO;
    previousTopBarStyle = [self navigationController].navigationBar.barStyle;
    previousToolbarHidden = [[self navigationController].toolbar isHidden];
    [[self navigationController].navigationBar setBarStyle:UIBarStyleBlackOpaque];
    [[self navigationController] setToolbarHidden:YES animated:YES];
    
    deleteMarks = [NSMutableArray arrayWithCapacity:10];
    NSUInteger count = [[ISBookProgressBO allBookProgressForBook:_bookName] count];
    for (NSUInteger i = 0; i<count; i++) {
        [deleteMarks addObject:[NSNumber numberWithBool:NO]];
    }

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    selectedProgress = nil;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[self navigationController].navigationBar setBarStyle:previousTopBarStyle];
    [[self navigationController] setToolbarHidden:previousToolbarHidden animated:YES];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[ISBookProgressBO allBookProgressForBook:_bookName] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"bookmarkCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell
    ISBookProgress *progress = [[ISBookProgressBO allBookProgressForBook:_bookName] objectAtIndex:[indexPath indexAtPosition:1]];
    cell.textLabel.text = progress.context;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%d%%", (int)( 100.f * [progress.progress floatValue])];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editing) {
        NSUInteger pos = [indexPath indexAtPosition:1];
        //Toggle delete switch
        [deleteMarks removeObjectAtIndex:pos];
        [deleteMarks insertObject:[NSNumber numberWithBool:YES] atIndex:pos];
    } else {
        //Save progress and quit.
        ISBookProgress *progress = [[ISBookProgressBO allBookProgressForBook:_bookName] objectAtIndex:[indexPath indexAtPosition:1]];
        selectedProgress = progress.progress;
        [[self navigationController] popViewControllerAnimated:YES];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editing) {
        NSUInteger pos = [indexPath indexAtPosition:1];
        //Toggle delete switch
        [deleteMarks removeObjectAtIndex:pos];
        [deleteMarks insertObject:[NSNumber numberWithBool:NO] atIndex:pos];
    }
}

- (void)viewDidUnload {
    [self setEditSwitchButton:nil];
    [super viewDidUnload];
}
@end
