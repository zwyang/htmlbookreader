//
//  main.m
//  HTMLBookReader
//
//  Created by celon on 6/11/12.
//  Copyright (c) 2012 celon. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ISAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ISAppDelegate class]));
    }
}
