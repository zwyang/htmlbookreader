//
//  ManagedObjectBO.h
//  NaviTest
//
//  Created by celon on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface ManagedObjectBO : NSObject{
@protected    
    NSString *entityName;
    NSString *dbFileName;
    NSManagedObjectContext *managedObjectContext;
    NSManagedObjectModel *managedObjectModel;
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
}

@property (nonatomic, retain) NSString *entityName;
@property (nonatomic, retain) NSString *dbFileName;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (ManagedObjectBO *)initWithEntityName:(NSString *)entity andDBFileName:(NSString *)fileName;
- (void) setUseCloud;

- (NSManagedObjectContext *)managedObjectContext;
- (NSManagedObjectModel *)managedObjectModel;
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator;
- (NSURL *)applicationDocumentsDirectory;

- (void) saveContextWithAsyncQueue:(BOOL)async;
- (void) lockContext;
- (void) unlockContext;

@end
