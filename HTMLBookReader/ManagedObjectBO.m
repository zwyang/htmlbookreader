//
//  ManagedObjectBO.m
//  NaviTest
//
//  Created by celon on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ManagedObjectBO.h"

@interface ManagedObjectBO () {
    pthread_mutex_t _context_mutex;
    @private
    bool useCloud;
}

@end

@implementation ManagedObjectBO

@synthesize entityName;
@synthesize dbFileName;
@synthesize managedObjectContext;
@synthesize managedObjectModel;
@synthesize persistentStoreCoordinator;

#pragma mark - init

- (ManagedObjectBO *) initWithEntityName:(NSString *)entity andDBFileName:(NSString *)fileName
{
    self=[super init];
    useCloud = NO;
    [self setEntityName:entity];
    [self setDbFileName:fileName];
    pthread_mutex_init(&_context_mutex, NULL);
    return self;
}

- (ManagedObjectBO *) initWithEntityName:(NSString *)entity andDBFileName:(NSString *)fileName useCloud:(BOOL)iCloudSpt
{
    self=[super init];
    useCloud = iCloudSpt;
    [self setEntityName:entity];
    [self setDbFileName:fileName];
    pthread_mutex_init(&_context_mutex, NULL);
    return self;
}

- (void) setUseCloud
{
    useCloud = YES;
}

- (void) lockContext
{
    pthread_mutex_lock(&_context_mutex);
}

- (void) unlockContext
{
    pthread_mutex_unlock(&_context_mutex);
}

#pragma mark -
#pragma mark core data function

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)saveContext_int
{
    NSError *error = nil;
    NSManagedObjectContext *objectContext = [self managedObjectContext];
    if (objectContext != nil)
    {
        [self lockContext];
        if ([objectContext hasChanges] && ![objectContext save:&error])
        {
            [self unlockContext];
            // add error handling here
            NSLog(@"Error occurred when saving data. code:%d\n domain:%@\n info:%@\n", [error code], [error domain], [error userInfo]);
            NSString *guiMsg = [NSString stringWithFormat:@"Error occurred when saving data. code:%d\n domain:%@\n", [error code], [error domain]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error occurred." message:guiMsg delegate:nil cancelButtonTitle:@"Oh!" otherButtonTitles:nil, nil];
            [alert show];
        } else
            [self unlockContext];
    }
}

- (void)saveContextWithAsyncQueue:(BOOL)async
{
    if (async) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,0), ^{
            [self saveContext_int];
        });
    } else {
        [self saveContext_int];
    }
}

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext
{
    
    if (managedObjectContext != nil)
    {
        return managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil)
    {
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return managedObjectContext;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel
{
    if (managedObjectModel != nil)
    {
        return managedObjectModel;
    }
    //managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    NSString *modelPath = [[NSBundle mainBundle] pathForResource:entityName ofType:@"momd"];
    if (!modelPath) {
        NSLog(@"model DB file %@ does not exist.", entityName);
    }
    NSURL *modelURL = [NSURL fileURLWithPath:modelPath];
    managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    return managedObjectModel;
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    
    if (persistentStoreCoordinator != nil)
    {
        return persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:dbFileName];
    
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }  
    
    return persistentStoreCoordinator;
}

@end
