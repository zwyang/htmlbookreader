//
//  ResourcesUtil.h
//  NaviTest
//
//  Created by celon on 21/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResourcesUtil : NSObject

+ (float) recoverProgress;
+ (void) setRecoverProgress:(float) progress;

+ (float) fontScale;
+ (void) setFontScale:(float) scale;

@end
