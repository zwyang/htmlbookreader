//
//  ISISBookProgress.h
//  HTMLBookReader
//
//  Created by celon on 7/11/12.
//  Copyright (c) 2012 celon. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface ISBookProgress : NSManagedObject

@property (nonatomic, retain) NSString *book_name;
@property (nonatomic, retain) NSString *context;
@property (nonatomic, retain) NSDate *modify_time;
@property (nonatomic, retain) NSNumber *progress;

@end
