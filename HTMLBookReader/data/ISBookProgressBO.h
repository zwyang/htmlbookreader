//
//  ISBookProgressBO.h
//  HTMLBookReader
//
//  Created by celon on 7/11/12.
//  Copyright (c) 2012 celon. All rights reserved.
//

#import "ManagedObjectBO.h"
#import "ISBookProgress.h"

@interface ISBookProgressBO : ManagedObjectBO

+ (ISBookProgress *) createBookProgressAt:(CGFloat)progress withContext:(NSString *)context inBook:(NSString *)bookName;
+ (NSArray *) allBookProgressForBook:(NSString *)bookName;
+ (void) deleteBookmark:(ISBookProgress *)progress;

@end
