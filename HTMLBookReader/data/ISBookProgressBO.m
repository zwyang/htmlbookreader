//
//  ISBookProgressBO.m
//  HTMLBookReader
//
//  Created by celon on 7/11/12.
//  Copyright (c) 2012 celon. All rights reserved.
//

#import "ISBookProgressBO.h"

@implementation ISBookProgressBO

+ (ISBookProgress *) createBookProgressAt:(CGFloat)progress withContext:(NSString *)context inBook:(NSString *)bookName
{
    return [[ISBookProgressBO instance] createBookProgressAt:progress withContext:context inBook:bookName];
}

- (ISBookProgress *) createBookProgressAt:(float)progress withContext:(NSString *)context inBook:(NSString *)bookName
{
    [self lockContext];
    ISBookProgress *instance = [NSEntityDescription insertNewObjectForEntityForName:ENTITY_NAME inManagedObjectContext:[self managedObjectContext]];
    [self unlockContext];
    [instance setBook_name:bookName?bookName:@""];
    [instance setContext:context?context:@""];
    [instance setModify_time:[NSDate date]];
    [instance setProgress:[NSNumber numberWithFloat:progress]];
    //save to DB
    [self saveContextWithAsyncQueue:YES];
    return instance;
}

+ (NSArray *) allBookProgressForBook:(NSString *)bookName
{
    return [[ISBookProgressBO instance] allBookProgressForBook:bookName];
}

- (NSArray *) allBookProgressForBook:(NSString *)bookName
{
    [self lockContext];
    NSEntityDescription *entityDesc=[NSEntityDescription entityForName:ENTITY_NAME inManagedObjectContext:[self managedObjectContext]];
    NSFetchRequest *req=[[NSFetchRequest alloc] init];
    
    [req setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"book_name", bookName];
    [req setPredicate:predicate];
    NSError *error;
    NSArray *array=[[self managedObjectContext] executeFetchRequest:req error:&error];
    [self unlockContext];
    return array;
}

+ (void) deleteBookmark:(ISBookProgress *)progress
{
    [[ISBookProgressBO instance] deleteBookmark:progress];
}

- (void) deleteBookmark:(ISBookProgress *)progress
{
    [[self managedObjectContext] deleteObject:progress];
    [[self managedObjectContext] save:nil];
}

#pragma mark - internal methods

static NSString *ENTITY_NAME=@"BookProgress";
static NSString *SQLITE_NAME=@"BookProgress.sqlite";
static ISBookProgressBO *_INSTANCE;

+ (ISBookProgressBO *) instance
{
    if (_INSTANCE == nil) {
        _INSTANCE = [[ISBookProgressBO alloc] init];
    }
    return _INSTANCE;
}

- (ISBookProgressBO *) init
{
    self = [super initWithEntityName:ENTITY_NAME andDBFileName:SQLITE_NAME];
    if (_INSTANCE == nil) {
        _INSTANCE = self;
    }
    return self;
}

+ (void) saveContextWithAsyncQueue:(BOOL)flag
{
    [[ISBookProgressBO instance] saveContextWithAsyncQueue:YES];
}

@end
