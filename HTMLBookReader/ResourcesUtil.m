//
//  ResourcesUtil.m
//  NaviTest
//
//  Created by celon on 21/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ResourcesUtil.h"

@interface ResourcesUtil() 
+ (void) initialValue;
@end

@implementation ResourcesUtil

static NSUserDefaults *userDefaults=nil;
static float recoverProgress;
static float fontScale;
static NSString *VALUE_YES  =@"YES";
static NSString *VALUE_NO   =@"NO";
static NSString *VALUE_PATCH_ID   =@"0";
static NSString *VALUE_NET_USAGE   =@"0";

static NSString *CFG_KEY_RECOVER_PROGRESS =@"CFG_RECOVER_PROGRESS";
static NSString *CFG_KEY_FONT_SCALE =@"CFG_FONT_SCALE";

+ (float) recoverProgress
{
    [self initialValue];
    return recoverProgress;
}

+ (void) setRecoverProgress:(float) progress
{
    [self initialValue];
    recoverProgress = progress;
    [userDefaults setFloat:progress forKey:CFG_KEY_RECOVER_PROGRESS];
}

+ (float) fontScale
{
    [self initialValue];
    return fontScale;
}

+ (void) setFontScale:(float) scale
{
    [self initialValue];
    fontScale = scale;
    [userDefaults setFloat:scale forKey:CFG_KEY_FONT_SCALE];
}

+ (void) initialDefaultStringValue:(NSString *)value WithKey:(NSString *)key
{
    NSString *tmpV=[userDefaults stringForKey:key];
    if (tmpV==nil) {
        [userDefaults setObject:value forKey:key];
    }else{
        value=tmpV;
    }
}

+ (void) initialValue
{
    static bool initialized=NO;
    if (initialized) {
        return;
    }
    
    if (userDefaults==nil) {
        userDefaults = [NSUserDefaults standardUserDefaults];
    }
    
    //load/set init values
    recoverProgress = [userDefaults floatForKey:CFG_KEY_RECOVER_PROGRESS];
    fontScale = [userDefaults floatForKey:CFG_KEY_FONT_SCALE];
    if (fontScale == 0) {
        if ([[UIDevice currentDevice].model isEqualToString:@"iPad"]) {
            fontScale = 1.728f;
        } else {
            fontScale = 1.0f;
        }
        [userDefaults setFloat:fontScale forKey:CFG_KEY_FONT_SCALE];
    }
    
    initialized=true;
}

@end
