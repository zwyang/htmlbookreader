//
//  ISAppDelegate.h
//  HTMLBookReader
//
//  Created by celon on 6/11/12.
//  Copyright (c) 2012 celon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
